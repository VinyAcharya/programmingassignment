import java.util.*;

//Functional Interface
interface rectangle{
    float area(float l,float b);
}

public class FuncInterface {

    public static void main(String args[])
    {
        rectangle Area=(l,b)->(l*b);
        System.out.println("Area of Rectangle is : "+Area.area(12.5f,30.6f));

    }
}
