

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CustomerId {
    public static void main(String args[]) {

        List<Integer> customerId = new ArrayList<>();
        for (int i = 1000; i <= 5000; i++) {
            customerId.add(i);
        }
        boolean start = true;
        while (start) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter CustomerID(1000 - 5000)");
            try {
                int searchElement = scan.nextInt();
                //boolean result=customerId.stream().anyMatch(element->element==(searchElement));
                if ((searchElement) >= 1000 && searchElement <= 5000) {
                    start = false;
                    boolean result = customerId.contains(searchElement);
                    if (result) {
                        System.out.println("Congratulations!!!");
                        System.out.println("Customer ID " + searchElement + " present ");
                    } else {
                        System.out.println("Better Luck Next Time!!!");
                        System.out.println("Customer ID " + searchElement + " Not Present !!!!!");
                    }
                } else {
                    System.out.println("Enter valid values !!");
                }
            } catch (Exception e) {
                System.out.println("Enter valid values !!" + e);

            }
        }
    }
}
