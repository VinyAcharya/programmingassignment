import java.util.*;

public class StringList {
    public static void main (String args[])
    {
        List<String> stringList=new ArrayList<String>(Arrays.asList("Violin","Guitar","Viola","Ukulele","Mandolin",
                "Harp","Sitar","Banjo","Ford India","Toyota Kirloskar"));


        //Printing the string and its Size
        stringList.forEach(listString->{
            System.out.println(listString+" || Length is : "+listString.length());
        });

    }
}

