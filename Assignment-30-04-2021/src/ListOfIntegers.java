import java.util.*;
public class ListOfIntegers {
    public static void main(String args[])
    {
        List<Integer> listOfInteger=new ArrayList<Integer>();
        int consInteger=100;
        //Populating list with consecutive numbers
        for(int i=1;i<=consInteger;i++)
        {
            listOfInteger.add(i);
        }

        //Printing numbers using for loop
        for(int i=1;i<100;i++)
        {
            System.out.println("Printing numbers using for loop");
            System.out.println(listOfInteger.get(i));
        }

        //Printing numbers using forEach loop
        for(Integer number:listOfInteger)
        {
            System.out.println("Printing numbers using Advanced for loop");
            System.out.println(number);
        }

        //Printing numbers using for Each
        System.out.println("Printing numbers using forEach");
        listOfInteger.forEach(number->System.out.println(number));
    }
}
