package TrainingDay1;

@FunctionalInterface
interface AllMethods
{
    public void display();

    public default void printDefault()
    {
        System.out.println("Default method Executing");
    }
    public static int element=10;
    public static void getStaticElement()
    {
        System.out.println(element); ;
    }
}
public class FunctionalInterWithAllMthods {
    public static void main(String[] main)
    {
        AllMethods getAbstract=()-> System.out.println("Abstract method Executing.");
        getAbstract.display();
        getAbstract.printDefault();
        AllMethods.getStaticElement();
    }
}
