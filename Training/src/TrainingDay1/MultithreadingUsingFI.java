package TrainingDay1;

public class MultithreadingUsingFI {
    public static void main(String[] args)
    {
        Runnable task1=()->{
            System.out.println(Thread.currentThread().getName()+" Running");
        };

        Runnable task2=()->{
            System.out.println(Thread.currentThread().getName()+" Running");
        };

        Thread thread1=new Thread(task1);
        Thread thread2=new Thread(task2);

        thread1.start();
        thread2.start();

    }
}
