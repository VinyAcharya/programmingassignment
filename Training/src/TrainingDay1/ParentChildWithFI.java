package TrainingDay1;

@FunctionalInterface
interface ParentInterface
{
   public void display();
}

@FunctionalInterface
interface ChildInterface extends ParentInterface
{

}
//Giving error for different abstract method in parent and child interface
/*If an interface extends java Functional Interface and child interface doesn&rsquo;t contain any abstract method
        then child interface is also Functional Interface.
        In this case In child interface we can&rsquo;t define any new abstract methods
        In the child interface we can define exactly same parent interface abstract method.*/


public class ParentChildWithFI {
    public static void main(String[] args)
    {
        ParentInterface parent=()-> System.out.println("Method Inside Parent Interface");
        ChildInterface child=()-> System.out.println("Method inside Child Interface");

    parent.display();
    child.display();
    }

}
