package TrainingDay1;

@FunctionalInterface
interface ParentInterfaceFI
{
    void display();

}

interface ChildInterfaceWoutFI extends ParentInterfaceFI
{

}

//When child interface extends parent functional interface child automatically becomes the functional interface
public class ParentChildWoutFI {
    public static void main(String[] main)
    {
        ParentInterfaceFI parent=()-> System.out.println("Method Inside Parent Interface ");
        ChildInterfaceWoutFI child=()-> System.out.println("Method Inside Child Interface");

        parent.display();
        child.display();
    }
}
