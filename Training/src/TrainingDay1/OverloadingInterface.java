package TrainingDay1;
/*

@FunctionalInterface
interface OverloadingParent
{
    public void add(int a,int b);
}

interface OverloadingChild extends OverloadingParent
{
    public void add(int a,int b,int c);
}

//In this case we are having two abstract methods
//Functional interface is the one which have only one Abstract methods.
//Method overload entirely two different methods they share only the names.
//Two overloaded methods requires two method bodies,but Functional Interface requires Single method body.
public class OverloadingInterface {
    public static void main(String[] args)
    {
        OverloadingParent addNumberParent=(a,b)-> System.out.println("Parent "+(a+b));
        OverloadingChild addNumberChild=(a,b,c)-> System.out.println("Child "+(a+b+c));

        addNumberParent.add(20,40);
        addNumberChild.add(10,20,20);

    }
}

*/
