@FunctionalInterface
interface Addition{
    int add(int a,int b);
   // int substract(int a,int b);
}

public class FuncInterfaceDemo {
    public static void main(String[] args) {
        Addition addInteger = (a, b) -> a + b;
        System.out.println(addInteger.add(10,20));
    }
}
