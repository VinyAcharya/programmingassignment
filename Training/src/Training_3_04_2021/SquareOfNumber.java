package Training_3_04_2021;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class SquareOfNumber {
    public static void main(String[] args)
    {
        List<Integer> listOfNumbers=new ArrayList<>();

        for(int i=0;i<10;i++)
        {
            listOfNumbers.add(new Random().nextInt(100));
        }
        System.out.println(listOfNumbers);

        List<Integer> squareOfNumbers=listOfNumbers.stream().map(number->number*number).collect(Collectors.toList());
        squareOfNumbers.forEach(System.out::println);
    }
}

