package Training_3_04_2021;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class IntegerSort {
    public static void main(String[] main)
    {
        List<Integer> listOfNumbers=new ArrayList<>();

        for(int i=0;i<10;i++)
        {
            listOfNumbers.add(new Random().nextInt(100));
        }
        System.out.println("Ascending Order");
        //Printing Ascending Order
        listOfNumbers.stream().sorted().forEach(System.out::println);

        System.out.println("Descending Order");
        //Printing Ascending Order
        listOfNumbers.stream().sorted(Comparator.reverseOrder()).forEach(System.out::println);



    }
}
