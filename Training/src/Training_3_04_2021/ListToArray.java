package Training_3_04_2021;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ListToArray {
    public static void main(String[] main) {
        List<Integer> listOfNumbers = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            listOfNumbers.add(new Random().nextInt(100));
        }
        System.out.println(listOfNumbers);
        //Converting from List to Integer
        int[] listToArray=listOfNumbers.stream().mapToInt(Integer::intValue).toArray();

        for(Integer number:listToArray)
        {
            System.out.println(number);
        }


    }
    }
