package Training_3_04_2021;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class StringOperation {
    public static void main(String[] args)
    {
        List<String> stringList=Arrays.asList("place","One","Charecter","ad","a","er","qw","finish","thirthahalli","Enter","message","terminal");

        //Printing the strings having length more than 3 charecters
        stringList.stream().filter(string->string.length()>3).forEach(System.out::println);
    }
}
