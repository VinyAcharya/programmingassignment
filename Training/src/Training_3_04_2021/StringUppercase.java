package Training_3_04_2021;

import java.util.Arrays;
import java.util.List;

public class StringUppercase {
    public static void main(String[] args)
    {
        List<String> stringList= Arrays.asList("place","One","Charecter","ad","a","er","qw","finish","thirthahalli","Enter","message","terminal");

        //Printing the strings with converting it to uppercase
        stringList.stream().map(string->string.toUpperCase()).forEach(System.out::println);
    }
}
