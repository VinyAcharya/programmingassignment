package Training_3_04_2021;

import java.util.Arrays;
import java.util.List;

public class CountOfString {
    public static void main(String[] args)
    {
        List<String> stringList= Arrays.asList("place","three","Charecter","ad","a");

        //Printing the count of strings having more than 3 charecter

        long stringCount=stringList.stream().filter(string->string.length()>3).count();
        System.out.println("String count having more than 3 charecters : "+stringCount);
    }
}
