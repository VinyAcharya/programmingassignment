package Training_3_04_2021;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class EvenUsingStreams {
    public static void main(String[] args)
    {
    List<Integer> evenList=new ArrayList<>();
    for(int i=0;i<30;i++)
    {
        evenList.add(new Random().nextInt(100));
    }
    //Printing only even Integers using Streams
       List onlyEvenList =evenList.stream().filter(s->s%2==0).collect(Collectors.toList());
        onlyEvenList.forEach(evenNumber-> System.out.println(evenNumber));
    }
}
