package Training_3_04_2021;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class MinMaxInteger {
    public static void main(String[] main)
    {
        List<Integer> listOfNumbers=new ArrayList<>();

        for(int i=0;i<10;i++)
        {
            listOfNumbers.add(new Random().nextInt(100));
        }

        //Printing minimum of Integer
        long minInteger=listOfNumbers.stream().min(Integer::compare).get();

        //Printing the maximum of Integer
        long maxInteger=listOfNumbers.stream().max(Integer::compare).get();
        System.out.println(listOfNumbers);
        System.out.println("Minimum of  Number list  is : "+minInteger);
        System.out.println("Maximum of Number list is : "+maxInteger);





    }
}
