interface InterF {
    public void m1(String a, String b);
}

public class FInterfaceDemo {
    public static void main(String[] args) {
//        InterF inter = (int a, int b) -> {
//            System.out.println(a + b);
//        };

        InterF inter = (a,b) ->
            System.out.println(a + b);
        inter.m1("ABC", "XYZ");



    }
}
