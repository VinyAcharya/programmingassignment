package assignment;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateDemo {
    public static void main(String[] args) {
        Predicate<Integer> evenInteger = (x) -> x % 2 == 0;
        List<Integer> listOfIntegers = Arrays.asList(2, 33, 45, 42, 32, 38, 65, 66, 54, 34, 31, 11, 22, 32, 30, 53, 46, 78);
       //Processing and listing only Even integer using Predicate
        List<Integer> evenList = listOfIntegers.stream().filter(evenInteger).collect(Collectors.toList());
        System.out.println(evenList);
    }
}
