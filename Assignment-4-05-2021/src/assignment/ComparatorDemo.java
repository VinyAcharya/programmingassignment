package assignment;

        import java.util.List;
        import java.util.Arrays;
        import java.util.Comparator;
        import java.util.stream.Collectors;

public class ComparatorDemo {
    public static void main(String[] args) {
        Comparator<Integer> ascendingOrder = (i1, i2) -> i1.compareTo(i2);
        Comparator<Integer> descendingOrder = (i1, i2) -> i2.compareTo(i1);

        List<Integer> listOfIntegers = Arrays.asList(2, 7, 5, 43, 5, 8, 6, 3, 0, 4, 9, 12);

        //Sorting in ascending Order
        List<Integer> ascendingOrderList = listOfIntegers.stream().sorted(ascendingOrder).collect(Collectors.toList());
        System.out.println(ascendingOrderList);
        //Sorting in Descending Order
        List<Integer> descendingOrderList = listOfIntegers.stream().sorted(descendingOrder).collect(Collectors.toList());
        System.out.println(descendingOrderList);
    }
}

 