package assignment;

public class Employee {
    private int empId;
    private String empName;
    private String surname;
    private String place;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Employee(int empId, String empName, String surname, String place)
    {
        this.empId=empId;
        this.empName=empName;
        this.place=place;
        this.surname=surname;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "empId=" + empId +
                ", empName='" + empName + '\'' +
                ", surname='" + surname + '\'' +
                ", place='" + place + '\'' +
                '}';
    }
}
