package assignment;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DistinctNumber {
    public static void main(String[] args)
    {
        List<Integer> duplicateList= Arrays.asList(1,3,4,3,4,5,6,2,8,7,9,5,6,7,4,3,2,1,3,4);
        //Without Sort Method
        List<Integer> uniqueList1=duplicateList.stream().distinct().collect(Collectors.toList());
        System.out.println(uniqueList1);
        //With sort method
        List<Integer> uniqueList=duplicateList.stream().sorted().distinct().collect(Collectors.toList());

        uniqueList.forEach(System.out::println);
    }
}
