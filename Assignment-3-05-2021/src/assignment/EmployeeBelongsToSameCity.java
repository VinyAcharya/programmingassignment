package assignment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.HashMap;

        public class EmployeeBelongsToSameCity {
            public static void main(String[] args) {

                List<Employee> employeeList = new ArrayList<>();

                //Adding employees to the list
                employeeList.add(new Employee(1, "Vinay","Kumar" ,"Pune"));
                employeeList.add(new Employee(2, "Viny", "Acharya","Banglore"));
                employeeList.add(new Employee(3, "Amith", "Kashyap","Bombay"));
                employeeList.add(new Employee(4, "Akshatha", "Patil","Rajasthan"));
                employeeList.add(new Employee(5, "Pranav","Sharma", "Tamilnadu"));
                employeeList.add(new Employee(6, "Venkat", "Prasad","Andrapradesh"));
                employeeList.add(new Employee(7, "Rahul", "Vittal","Lucknow"));
                employeeList.add(new Employee(8, "Vivek", "Mithal","Manglore"));
                employeeList.add(new Employee(9, "Sachin","Koparde", "Pune"));

                List<String> cities = Arrays.asList("Pune","Banglore","Bombay","Rajasthan","Tamilnadu",
                        "Andrapradesh","Lucknow","Manglore","Gadag");

                //Printing the employee belongs to only particular city
               //for(String city:cities) {

                Scanner scan=new Scanner(System.in);
                System.out.println("Enter the City Name : ");
                String city=scan.next();
                   employeeList.stream().filter(employee1 ->
                           employee1.getPlace().equals(city)).forEach((employee) ->
                           System.out.println(employee.getEmpName()+" Belongs To : "+city));
               //}
            }

}
