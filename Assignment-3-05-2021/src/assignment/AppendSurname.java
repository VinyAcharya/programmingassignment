package assignment;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.Map;

public class AppendSurname {
    public static void main(String[] args) {
        List<Employee> employeeList = new ArrayList<>();

        //Adding employees to the list
        employeeList.add(new Employee(1, "Vinay", "Kumar", "Pune"));
        employeeList.add(new Employee(2, "Viny", "Acharya", "Banglore"));
        employeeList.add(new Employee(3, "Amith", "Kashyap", "Bombay"));
        employeeList.add(new Employee(4, "Akshatha", "Patil", "Rajasthan"));
        employeeList.add(new Employee(5, "Pranav", "Sharma", "Tamilnadu"));
        employeeList.add(new Employee(6, "Venkat", "Prasad", "Andrapradesh"));
        employeeList.add(new Employee(7, "Rahul", "Vittal", "Lucknow"));
        employeeList.add(new Employee(8, "Vivek", "Mithal", "Manglore"));
        employeeList.add(new Employee(9, "Sachin", "Koparde", "Gadag"));

        List<String> EmployeeFirstName = new ArrayList<>();
        List<String> EmployeeLastName = new ArrayList<>();

        //Adding employee first name to the list
        for (Employee employee : employeeList)
            EmployeeFirstName.add(employee.getEmpName());

        for (Employee employee : employeeList)
            EmployeeLastName.add(employee.getSurname());

        //Printing first names
        System.out.println(EmployeeFirstName);

//        //Adding surname to the first name
//        for(String firstName:EmployeeFirstName)
//       employeeList.stream().filter(employee->employee.getEmpName().equals(firstName))
//                .map(employee->firstName+" "+employee.getSurname()).forEach(System.out::println);

        //Adding first name and surname from POJO Class
        System.out.println("Adding first name and surname from POJO Class");
        List<String> fullnames = employeeList.stream()
                .map(employee -> employee.getEmpName() + " " + employee.getSurname()).collect(Collectors.toList());
        fullnames.forEach(System.out::println);

        System.out.println("--------------------------------------------------------");
        System.out.println("Merging Names of Two List");
        List<String> fullname = IntStream.range(0, EmployeeFirstName.size())
                .mapToObj(i -> EmployeeFirstName.get(i) + " " + EmployeeLastName.get(i))
                .collect(Collectors.toList());

        fullname.forEach(System.out::println);

    }
}
