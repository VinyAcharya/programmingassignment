package assignment;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class EvenIntegers {
    public static void main(String[] args)
    {
        List<Integer> listOfIntegers=new ArrayList<>();

        for(int i=0;i<50;i++)
        {
            listOfIntegers.add(new Random().nextInt(300));
        }

        List<Integer> evenIntegers=listOfIntegers.stream().filter(number->number%2==0).collect(Collectors.toList());

        evenIntegers.forEach(System.out::println);

    }
}
